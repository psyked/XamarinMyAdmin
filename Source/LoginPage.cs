﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace App3
{
    class LoginPage : ContentPage
    {
        private Entry login, passwd, databaseUrl, databasePort, databaseTable;
        
        private Button connect;
        private Label label;
        private App parent;
        private ActivityIndicator loading;
        public LoginPage(App app)
        {
            parent = app;
            login = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Login",
                PlaceholderColor = Color.FromHex("FF6A00"),
                Text = "root"

            };

            passwd = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Password",
                PlaceholderColor = Color.FromHex("FF6A00"),
                IsPassword = true,
                Text = "root"
            };

            databaseUrl = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Database URL",
                PlaceholderColor = Color.FromHex("FF6A00"),
                Text = "192.168.1.50"

            };

            connect = new Button
            {
                Text = "Connection",
                Font = Font.SystemFontOfSize(NamedSize.Large),
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            connect.Clicked += onConnectClicked;

            label = new Label
            {
                Font = Font.SystemFontOfSize(NamedSize.Large),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.EndAndExpand,
                Text = ""
            };

            databaseTable= new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Table",
                PlaceholderColor = Color.FromHex("FF6A00"),
            };
        
            databasePort = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Port",
                PlaceholderColor = Color.FromHex("FF6A00"),
                Text = "3306"
            };
            loading = new ActivityIndicator
            {
                IsRunning = false,
                IsVisible = true,
                Color = Color.FromHex("FF6A00")
            };
            this.Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children = {
                           loading, login, passwd, databaseUrl, databasePort, databaseTable, connect, label
                        }
            };
        }

        public void onConnectClicked(object sender, EventArgs e)
        {
            label.Text = "Login field empty";
            if (login.Text == "" || login.Text == null)
            {
                Console.WriteLine("login empty");
                label.Text = "Login field empty";
            }
            /*else if (passwd.Text == "" || passwd.Text == null)
            {
                Console.WriteLine("password empty");
                label.Text += "\r\n" + "Password field empty";
            }*/
            else if (databaseUrl.Text == "" || databaseUrl.Text == null)
            {
                Console.WriteLine("database url empty");
                label.Text += "\r\n" + "Database URL missing";
            }
            else if (databasePort.Text == "" || databasePort.Text == null)
            {
                Console.WriteLine("database port empty");
                label.Text += "\r\n" + "Database port missing";
            }
            else if (databaseTable.Text == "" || databaseTable.Text == null)
            {
                Console.WriteLine("database table empty");
                label.Text += "\r\n" + "Database Table missing";
            }
            else
            {
                loading.IsVisible = true;
                loading.IsRunning = true;
                //TODO, connect to database and retrieve data
                //Connection with IP/URL, port, DB, user and password
                MySqlConnection sqlConnection = new MySqlConnection(); 
                string retourConnexion = SQLConnection.createConnection(sqlConnection, databaseUrl.Text, "3306", "xamarintest", login.Text, passwd.Text);
                //Launch request
                List<List<string>> listeRetour = SQLConnection.SendQueryToSQLDB(sqlConnection, "SELECT * FROM users");               
                //Close connection
                SQLConnection.closeConnection(sqlConnection);
                // /!\ Open connection and close it only on login page, keep connection activated as long as possible

                List<DBElement> items = new List<DBElement>();
                List<string> keys = listeRetour[0];
                Console.WriteLine(keys.ToString());
                foreach(List<string> row in listeRetour)
                {
                    Console.WriteLine(row.ToString());
                    DBElement item = new DBElement(keys, row);
                    items.Add(item);
                }


                /*
                //test
                //###################################################
                List<string> values = listeRetour[1]; //new List<string>() { "v1", "v2", "v3" };
                List<string> v2 = listeRetour[2]; //new List<string>() { "v1", "v2", "v3" };
                List<string> v3 = listeRetour[3]; //new List<string>() { "v1", "v2", "v3" };

                Console.WriteLine(keys[0]);
                /*Dictionary<string, List<string>> datareturn = new Dictionary<string, List<string>>();
                datareturn["name"] = new List<string>(new string[] { "pseudo", "age", "sexe" });
                datareturn["Ed"] = new List<string>(new string[] { "psyked", "21", "M" });
                datareturn["Hugo"] = new List<string>(new string[] { "tuture", "21", "M" });
                datareturn["Hugette"] = new List<string>(new string[] { "maison", "18", "F" });

                DBElement item1 = new DBElement(keys, values);
                DBElement item2 = new DBElement(keys, v2);
                DBElement item3 = new DBElement(keys, v3);
                items.Add(item1);
                items.Add(item2);
                items.Add(item3);
                //#################################################
                */
                string mainKey = keys[0]; // By default, first key in table
                
                DBMainPage dbMainPage = new DBMainPage(mainKey, items, parent, sqlConnection);

            }
        }
    }
}
