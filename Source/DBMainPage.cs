﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace App3
{
    public class DBMainPage : ContentPage
    {
        private DBlist dbList { get; set; }
        private Entry query { get; set; }
        private Button sendQuery;
        private App parent;
        private string selectedKey;
        private MySqlConnection mySqlConnection;
        public DBMainPage(string key, List<DBElement> items, App app, MySqlConnection sqlconn)
        {
            this.mySqlConnection = sqlconn;
            this.parent = app;
            this.selectedKey = key;
            this.dbList = new DBlist(this.selectedKey, items, this);
            query = new Entry
            {
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.FromHex("FF6A00"),
                Placeholder = "Query",
                PlaceholderColor = Color.FromHex("FF6A00")
            };
            sendQuery = new Button
            {
                Text = "Send",
                Font = Font.SystemFontOfSize(NamedSize.Small),
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            sendQuery.Clicked += SendQuery_Clicked;

            Content = new StackLayout
            {
                Children = {
                    query, sendQuery, dbList
                }
            };
            parent.MainPage = this;
        }

        private void SendQuery_Clicked(object sender, EventArgs e)
        {
            this.dbList = ConnectionEstablishment.sendQuery(null);
            SQLConnection.SendQueryToSQLDB(mySqlConnection, query.Text);
            throw new NotImplementedException();
        }
    }
}
