﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using Xamarin.Forms;
namespace App3
{
    class DBlist : ListView
    {
        private List<DBElement> items;
        private Page parent;
        private string selectedKey;
        public DBlist(string key, List<DBElement> items, Page page)
        {
            this.parent = page;
            this.items = items;
            this.selectedKey = key;

            DataTemplate dt = new DataTemplate(typeof(TextCell));
            dt.SetBinding(TextCell.TextProperty, "mainValue");
            this.ItemTemplate = dt;
            this.ItemsSource = items;
            
            this.ItemSelected += DBlist_ItemSelected;
        }

        private void DBlist_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            itemInfo info = new itemInfo(e.SelectedItem as DBElement);
            parent.DisplayAlert("Info", info.message, "Return");
        }
    }
}


