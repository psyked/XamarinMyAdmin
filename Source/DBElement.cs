﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App3
{
    public class DBElement
    {
        public StringDictionary keyValue { get; set; }
        public string mainValue{ get; set; }
        public DBElement(List<string> keys, List<string> values)
        {
            if (keys != null && values != null)
            {
                
                Console.WriteLine("keys and values != null");
                this.keyValue = new StringDictionary();
                var keysAndValues = keys.Zip(values, (k, v) => new { Key = k, Value = v });
                foreach (var kv in keysAndValues)
                {
                    Console.WriteLine(kv.Key + "  " + kv.Value);
                    keyValue.Add(kv.Key, kv.Value);
                }
                mainValue = values[0];
            }
        }
    }
}
