﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;
using System.Collections;
namespace App3
{
    class itemInfo : Page
    {
        private DBElement elem;
        public string message = "";
        public itemInfo(DBElement elem)
        {
            this.elem = elem;
            if (elem != null && elem.keyValue != null)
            {
                foreach (DictionaryEntry col in elem.keyValue)
                {
                    message += col.Key + " : " + col.Value;
                }
            }
            else
            {
                message = "Empty";
            }
        }
    }
}
