﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using I18N.West;

namespace App3
{
    class SQLConnection
    {
        public static bool closeConnection(MySqlConnection sqlconn)
        {
            try
            {
                string debug = "try to close connection";
                sqlconn.Close();
            }
            catch (System.Exception e)
            {
                return false;
            }
            return true;
        }
        public static string createConnection(MySqlConnection sqlconn, string ip, string port, string database, string user, string password)
        {
            try
            {
                string debug = "";
                new I18N.West.CP1250();
                Console.WriteLine("test : creation connexion");
                string connsqlstring = $"Server={ip};Port={port};database={database};User Id={user};Password={password};charset=utf8";
                Console.WriteLine("test : " + connsqlstring);
                //TODO, Try catch block to prevent exception and display error message in LoginPage.label

                Console.WriteLine("test : test co");
                sqlconn = new MySqlConnection(connsqlstring);
                

                
                debug += "connection ? ";
                sqlconn.Open();
                Console.WriteLine("test : connected");
                debug += "ok ";
                return "connected";

            }
            catch (Exception e)
            {
                Console.WriteLine("test : " + e.ToString());
                return e.ToString();
            }            
        }
        private static string queryReturn(string query)
        {
            if (query.ToUpper().Contains("SELECT"))
            {
                return null;
            }
            string table = "";
            var parts = query.Split(' ');
            int i = 0;
            foreach (string el in parts)
            {
                i++;
                if (el.ToUpper().Equals("FROM"))
                {
                    table = parts[i];
                    break;
                }
            }
            if (query.ToUpper().Contains("UPDATE"))
            {
                return "SELECT * FROM " + table;
            }
            if (query.ToUpper().Contains("DELETE"))
            {
                return "SELECT * FROM " + table;
            }
            return null;
        }
        public static List<List<string>> SendQueryToSQLDB(MySqlConnection sqlconn, string queryString)
        {
            try
            {              
                DataSet tickets = new DataSet();
                MySqlDataAdapter adapter = new MySqlDataAdapter(queryString, sqlconn);
                if (queryReturn(queryString) != null)
                {
                    adapter = new MySqlDataAdapter(queryReturn(queryString), sqlconn);
                }
                adapter.Fill(tickets);

                List<List<string>> listreturn = new List<List<string>>();
                foreach (DataTable table in tickets.Tables)
                {
                    List<string> elem = new List<string>();
                    foreach (DataColumn col in table.Columns)
                    {
                        elem.Add(col.ToString());
                    }
                    listreturn.Add(elem);

                    foreach (DataRow row in table.Rows)
                    {
                        List<string> elm = new List<string>();
                        foreach (DataColumn column in table.Columns)
                        {
                            if (row[column] != null)
                            {
                                elm.Add(row[column].ToString());
                            }
                        }
                        listreturn.Add(elm);
                    }
                }
                /*string sreturn = "";
                foreach (List<string> l in listreturn)
                {
                    foreach (string s in l)
                    {
                        sreturn += s + " ";
                    }
                    sreturn += "\n";
                }
                System.Console.WriteLine(sreturn);*/
                return listreturn;
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
