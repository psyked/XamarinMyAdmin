<h1>XamarinMyAdmin</h1><br/>
<h2>What's the point of the application ?</h2>
<p>
XamarinMyAdmin is an Android application used to connect a user from the network to his database.
</p>
<h2>How to use our application ?</h2>
<p>
Once the application started, you have to fill main entries in order to establish connection to your database. 
By default, the query executed on a successful connection is "SELECT * from [table]".<br/>
Once connected, each row of the table is stored as an object in a list. The value of the first key is displayed on the screen into a list.
If you click on a value, it will display keys and values corresponding to the object.<br/>
There is a query entry in which you can execute SQL queries. It will refresh the list displayed on screen on success.<br/>
</p>
<h2>How to use the sources ?</h2>
<p>
Just create a new project Xamarin Form for Android, import the c# files, replace App.cs and include as References MySql.Data.CF.dll.<br/>
Then you just have to build the project and retrieve the app package. <br/>
We used a Xamarin form so you can build this project on iOS by getting MySql.Data.CF.dll for this OS <a href=https://components.xamarin.com/view/mysql-plugin>here</a> then
build the project on a mac device. You can build the project too for UWP by replacing the MySql package by the MySQL package. And then run on most mobile devices
</p>
<p>
NB : You will have to authorise, in your SQL configuration, a user on the network you want to reach from (Ex : root@192.168.0.0).
</p>